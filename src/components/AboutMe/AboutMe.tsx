export default function AboutMe() {
  return (
    <div className="hero">
      <h2 className="logo">Personal Website</h2>

      <li>
        <a href="#">Home</a>
      </li>
      <li>
        <a href="#about">About Me</a>
      </li>
      <li>
        <a href="#">Experience</a>
      </li>
      <li>
        <a href="#">Skills</a>
      </li>
      <li>
        <a href="#">Photos</a>
      </li>
      <li>
        <a href="#">Contact Us</a>
      </li>
      <section id="about">
        <div className="main about_main"> </div>
        <img src="http://www.tv-tokyo.co.jp/contents/aniyoko/images/pic_staff.gif" />
        <div className="about-text">
          <h2>About Me</h2>
          <h5>Year 2, Data Science and Policy Studies </h5>
          <p>
            I am a University Student studying in the Chinese University of Hong
            Kong.
          </p>
        </div>
      </section>

      <section id="about">
        <div className="main about_main"> </div>
        <img src="http://www.tv-tokyo.co.jp/contents/aniyoko/images/pic_staff.gif" />
        <div className="about-text">
          <h2>About Me</h2>
          <h5>Year 2, Data Science and Policy Studies </h5>
          <p>
            "I am a University Student studying in the Chinese University of
            Hong Kong."
          </p>
        </div>
      </section>
    </div>
  );
}
